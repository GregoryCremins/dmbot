# Map
# all classes and related methods for the DM bot map functionlity

# consume json from a supplied file and convert it into a dictionary
# return the dictionary to the caller

import json
from os import path
from random import randint
import random
from Model.Space import Space
from Model.Space import getSpace
from Model.Actor import Actor
from Model.Actor import giveActor
from Model.Space import _generateRandomHazard

def mapParser(mapPath):
    #consume filename to make map
    data = {}
    #make sure that the template map exists at all

    try:
        with open(mapPath, "r") as read_file:
            data = json.load(read_file)
    except OSError as e: # FileNotFound or File can't be opened for reading
        print(f'File cannot be opened: {mapPath}')
        print(e)
        raise

    for k, v in data.items():
        del data[k]
        if type(v) is str:
            data[k] = v.upper()
        else:
            x = [i.upper() for i in v]
            data[k] = x

    return data

class Room:

    def __init__(self, inlist):
        self.sdict = {}
        self.count = 0
        for x,y in inlist:
            self.sdict[str(self.count)] = (x, y)
            self.count +=1

    def randSpace(self):
        return self.sdict[str(random.randint(0, self.count-1))]

    def getRoomSpaces(self):
        return self.sdict.values()



class Map:

    def __init__(self, indict, adict):
        # constructor for when we have an input dictionary
        # parse out the dictionary into its component peices and use those to call the map generator
        print(indict)
        mapSize = indict["SIZE"]
        monsterDensity = indict["MONSTERS"]
        mapTypeList = indict["TYPE"]
        mapDangerZone = indict["HAZARDS"]
        # call getspace(list of type)
        self.mapX = 0
        self.mapY = 0
        if mapSize == "L":
            self.mapX = randint(30, 40)
            self.mapY = randint(30, 40)
        elif mapSize == "M":
            self.mapX = randint(20,30)
            self.mapY = randint(20,30)
        else:
            self.mapX = randint(10, 20)
            self.mapY = randint(10, 20)


        # make the dummy binary map
        self.dmap = [["X" for y in range(self.mapY)]for x in range(self.mapX)]
        for x in range(1, self.mapX-1):
            for y in range(1, self.mapY-1):
                if randint(1, 100) > 65:
                    self.dmap[x][y] = " "
        # poked holes in the map
        # enlargenify holes
        room = 1
        for x in range(2, self.mapX-2):
            for y in range(2, self.mapY-2):
                tcount = 0
                for a, b in [(x, y), (x+1, y), (x-1, y), (x, y+1), (x, y-1)]:
                    if self.dmap[a][b] ==" ":
                        tcount+=1
                if tcount >= 3:
                    for a, b in [(x, y), (x+1, y), (x-1, y), (x, y+1), (x, y-1)]:
                        self.dmap[a][b] = "L"

        #enlargified the holes
        for x in range(0, self.mapX):
            for y in range(0, self.mapY):
                if self.dmap[x][y] == "L":
                    self.dmap[x][y] = " "

        #clean out the too small holes
        for x in range(1, self.mapX-1):
            for y in range(1, self.mapY-1):
                if self.dmap[x][y] !="X" and self.dmap[x][y-1] == "X" and self.dmap[x][y+1] == "X":
                    self.dmap[x][y] = "X"

        for x in range(1, self.mapX-1):
            for y in range(1, self.mapY-1):
                if self.dmap[x][y] !="X" and self.dmap[x-1][y] == "X" and self.dmap[x+1][y] == "X":
                    self.dmap[x][y] = "X"

        # clean out the too thin walls
        for x in range(1, self.mapX-1):
            for y in range(1, self.mapY-1):
                if self.dmap[x][y] =="X" and self.dmap[x][y-1] == " " and self.dmap[x][y+1] == " ":
                    self.dmap[x][y] = " "

        for x in range(1, self.mapX-1):
            for y in range(1, self.mapY-1):
                if self.dmap[x][y] =="X" and self.dmap[x-1][y] == " " and self.dmap[x+1][y] == " ":
                    self.dmap[x][y] = " "

        #collect rooms because we want them
        room = 0
        for x in range(0, self.mapX):
            for y in range(0, self.mapY):
                if self.dmap[x][y] ==" ":
                    self.fill([(x+1, y), (x-1, y), (x, y+1), (x, y-1)], room)
                    room = room+1

        # rooms are now numbers, now what
        roomlist = []
        for i in range(0, room):
            rslist = []
            for x in range(0, self.mapX):
                for y in range(0, self.mapY):
                    if self.dmap[x][y] == str(i):
                        rslist.append((x, y))

            roomlist.append(Room(rslist))
        #print(roomlist[0].randSpace())
        for i in range(0, len(roomlist)-1):
            self.juggernaught(roomlist[i].randSpace(), roomlist[i+1].randSpace())
        self.juggernaught(roomlist[len(roomlist)-1].randSpace(), roomlist[0].randSpace())

        # juggernaught from 1 to 2 to 3 to 4

        self.space = {}
        for x in range(0, self.mapX):
            for y in range(0, self.mapY):
               if self.dmap[x][y] !="X" and self.dmap[x][y] != "@":
                    foundTiles = (roomlist[int(self.dmap[x][y])]).getRoomSpaces()
                    splist = getSpace(mapTypeList[0], num_spaces = len(foundTiles))
                    t = 0
                    for a,b in foundTiles:
                        self.space[(a,b)] = splist[t]
                        t+=1

        for x in range(0, self.mapX):
            for y in range(0, self.mapY):
               if self.dmap[x][y] == "@":
                    splist = getSpace(mapTypeList[0], num_spaces = 1)
                    self.space[(x, y)] = splist[0]

        # process the list of actor dictioonaries
        self.acmap = {}
        for e in adict:
            E = e[1]
            if E["FRIEND"]== "True":
                z = giveActor(E, 1)
                self.acmap[E["NAME"]] = z[0]
                k = roomlist[0].randSpace()
                while True:
                    if (self.space[(k[0],k[1])].getOccupied()) != True:
                        self.space[(k[0],k[1])].setOccupied(True)
                        self.acmap[E["NAME"]].setCoordinates(X=k[0], Y=k[1])
                        break

            else:
                mc = 5
                if monsterDensity == "many":
                    mc = 7
                elif monsterDensity == "lots":
                    mc = 10
                elif monsterDensity == "too many":
                    mc = 15
                else:
                    mc = 5
                z = giveActor(E, mc)
                for n in z:
                    self.acmap[n.getName()] = n
                    k = roomlist[randint(0,len(roomlist)-1)].randSpace()
                    while True:
                        if (self.space[(k[0],k[1])].getOccupied()) != True:
                            self.space[(k[0],k[1])].setOccupied(True)
                            cord = {"X":k[0], "Y":k[1]}
                            self.acmap[n.getName()].setCoordinates(cord)
                            break
                        
        # place the hazards on the board
        self.dangerCount = randint(3, 5)
        if mapDangerZone == "dense":
            self.dangerCount = randint(10, 15)
        elif self.mapDangerZone == "sparse":
            self.dangerCount = randint(3, 5)
        elif self.mapDangerZone == "safe":
            self.dangerCount = randing(0, 3)
        else:
            self.dangerCount = randint(15, 20)
            
        
        for i in range(self.dangerCount):
            d = random.choice(list(self.space.keys()))
            x = _generateRandomHazard(1, self.space[(d[0],d[1])])
            self.space[(d[0],d[1])].setHazards(x)
                     

        #cleanup
        for x in range(0, self.mapX):
            for y in range(0, self.mapY):
                if self.dmap[x][y] !="X":
                    self.dmap[x][y] = " "


    def __str__(self):
        strong =""
        for x in range(self.mapX):
            for y in range(self.mapY):
                strong = strong+self.dmap[x][y]
            strong = strong+"\n"
        return strong

    def getMapSpace(self, ugh):
        try:
             return self.space[(ugh[0],ugh[1])]
        except KeyError as e:
            return None
    def getActors(self):
        return self.acmap


    def juggernaught(self, pair1, pair2):
        x1 = pair1[0]
        y1 = pair1[1]
        x2 = pair2[0]
        y2 = pair2[1]
        if random.choice(list(["rise", "run"])) == "rise":
            for y in range(min(y1, y2)-1,max(y1, y2)+1):
                self.dmap[x1][y] = "@"
            for x in range(min(x1, x2)-1, max(x1, x2)+1):
                self.dmap[x][y1] = "@"
        else:
            for x in range(min(x1, x2)-1, max(x1, x2)+1):
                self.dmap[x][y1] = "@"
            for y in range(min(y1, y2)-1,max(y1, y2)+1):
                self.dmap[x1][y] = "@"
        return

    # Oh God Why
    # plz not this
    # import sludge for full effect
    def fill(self, spacelist, number):
        for x, y in spacelist:
            if self.dmap[x][y] == " ":
                self.dmap[x][y] = str(number)
                self.fill([(x+1, y), (x-1, y), (x, y+1), (x, y-1)], number)


    def moveActor(self,actor_name,x_coord,y_coord):
        out_msg = ""

        #out of bounds checkMove
        if x_coord >= self.mapX or x_coord < 0:
            return False, "ERROR: x coordinate out of bounds."
        if y_coord >= self.mapY or y_coord < 0:
            return False, "ERROR: y coordinate out of bounds."

        #check if occupado
        if self.space[(x_coord,y_coord)].getOccupied():
            return False, "ERROR: space you are trying to enter is occupied"

        #check status if player can move
        if (actor_name not in self.getActors()):
            return False, "ERROR: actor does not exist"

        oldposition = self.acmap[actor_name].getCoordinates()
        self.space[(oldposition['X'],oldposition['Y'])].setOccupied(False)
        self.space[(x_coord,y_coord)].setOccupied(True)
        self.acmap[actor_name].setCoordinates({'X':x_coord, 'Y': y_coord})



        return True, (
            "Successfully moved to (" + str(x_coord) + ", " + str(y_coord) + ")"
        )
