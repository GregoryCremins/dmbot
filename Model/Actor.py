import json

class Actor:
    def __init__(self, name,
        friend = False,
        max_movement = 30,
        curr_movement = 30,
        status = "OK",
        special = "Yes i am",
        **coordinates):

        self.name = name
        self.coordinates = coordinates
        self.friend = friend
        self.max_movement = max_movement
        self.curr_movement = curr_movement
        self.status = status
        self.special = special

    # Get name value
    def getName(self):
        return self.name

    # Set name value
    def setName(self,new_name):
        self.name = new_name

    # Get coordinate value
    def getCoordinates(self):
        return self.coordinates

    # Set coordinate value
    def setCoordinates(self,new_coordinates):
        self.coordinates = {
            "X" : new_coordinates["X"],
            "Y" : new_coordinates["Y"]
        }

    # Get friend value
    def getFriend(self):
        return self.friend

    # Set friend value
    def setFriend(self,new_friend):
        self.friend = new_friend

    # Get max movement value
    def getMaxMovement(self):
        return self.max_movement

    # Set max movement value
    def setMaxMovement(self,new_max_movement):
        self.max_movement = new_max_movement

    # Get curr movement
    def getCurrMovement(self):
        return self.curr_movement

    # Set curr movement
    def setCurrMovement(self,new_curr_movement):
        self.curr_movement = new_curr_movement

    # Get status value
    def getStatus(self):
        return self.status

    # Set status value
    def setStatus(self,new_status):
        self.status = new_status.upper()

    # Get special status
    def getSpecial(self):
        return self.special

    # Set special value
    def setSpecial(self,new_special):
        self.special = new_special.upper()

    # $$$ Do we need this or should it be here?
    # $$$ Also what is the space that it takes?
    def checkMove(test_space):
        if (status == "IMMOBILE" or status == "STUNNED"):
            return false
        else:
            return true

    # $$$ Could we make this the __str__() function so we could just
    # print(Actor)?
    def __str__(self):
        return_str = f'Name : {self.name}\n'
        return_str += f'Coordinates : {self.coordinates}\n'
        return_str += f'Friend : {self.friend}\n'
        return_str += f'Max Movement : {self.max_movement}\n'
        return_str += f'Movement Remaining : {self.curr_movement}\n'
        return_str += f'Status : {self.status}\n'
        return_str += f'Other : {self.special}\n'
        return return_str

# Parse Actor json into dictionary
def parseActorFromFile(filename):
    #testActor = Actor(name="BAD",coordinates={"X":-1,"Y":-1})
    testActor = {}
    with open(filename) as json_file:
        data = json.load(json_file)
        #check for bad coordinates or bad name
        if("NAME" not in data or type(data['NAME']) != str or data['NAME'].strip() == ""):
            return "Error: name invalid or missing", {}

        testActor['NAME'] =data['NAME']
        testActor['COORDINATES'] = {}

        if("COORDINATES" not in data or
        "X" not in data['COORDINATES'] or
        "Y" not in data['COORDINATES'] or
        type(data['COORDINATES']['X']) != int or
        type(data['COORDINATES']['Y']) != int
        ):
            #testActor.setCoordinates({"X":-1,"Y":-1})
            testActor['COORDINATES']['X'] = -1
            testActor['COORDINATES']['Y'] = -1
        else:
            #testActor.setCoordinates(data['COORDINATES'])
            testActor['COORDINATES']['X'] = data['COORDINATES']['X']
            testActor['COORDINATES']['Y'] = data['COORDINATES']['Y']

        #self.coordinates['Y'] = data['COORDINATES']['Y']
        # check other parameters
        if("FRIEND" not in data or
        type(data['FRIEND'] != bool)):
            #testActor.setFriend(True)
            testActor['FRIEND'] = True
        else:
            #testActor.setFriend(data['FRIEND'])
            testActor['FRIEND'] = data['FRIEND']

        if("MAX_MOVEMENT" not in data or
        type(data['MAX_MOVEMENT'] != int) or
        data['MAX_MOVEMENT'] < 0):
            #testActor.setMaxMovement(30)
            testActor['MAX_MOVEMENT'] = 30
        else:
            #testActor.setMaxMovement(data['MAX_MOVEMENT'])
            testActor['MAX_MOVEMENT'] = data['MAX_MOVEMENT']

        if("CURR_MOVEMENT" not in data or
        type(data['CURR_MOVEMENT'] != int) or
        data['CURR_MOVEMENT'] < 0):
            #testActor.setCurrMovement(20)
            testActor['CURR_MOVEMENT']=20
        else:
            #testActor.setCurrMovement(data['CURR_MOVEMENT'])
            testActor['CURR_MOVEMENT']=data['CURR_MOVEMENT']

        if("STATUS" not in data or
        data['STATUS'] == "None" or
        data['STATUS'].strip() == ""):
            #testActor.setStatus("OK")
            testActor["STATUS"]="OK"
        else:
            #testActor.setStatus(data['STATUS'])
            testActor["STATUS"]=data['STATUS']

        if("SPECIAL" not in data or
        data['SPECIAL'] == "None" or
        data['SPECIAL'].strip() == ""):
            #testActor.setSpecial("meh")
            testActor['SPECIAL']="meh"
        else:
            #testActor.setSpecial(data['SPECIAL'])
            testActor['SPECIAL']=data['SPECIAL']

        return "Successfully created actor " , testActor
#a = Actor("JERRY",coordinates = {"X":1,"Y":5})
#print(a.printActor())
#print(a.getStatus())

# Generate Actors
def giveActor(actor, count = 1):
    actors = []
    for i in range(1, count+1):
        name = actor['NAME']
        if count > 1:
            name += str(i)
        actors.append(
            Actor(name, X = 0, Y = 0)
        )
    return actors
