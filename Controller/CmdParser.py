def parseACTI(my_map, cmd_args):
    try:
        actors = my_map.getActors()
        if cmd_args[0] in actors:
            return str(actors[cmd_args[0]]), None
        else:
            return None, 'Error: Actor not found'
    except IndexError: # Empty args
        return None, 'Error: No actor provided\nUsage: ACTI [actor name]'


def parseSPCI(my_map, cmd_args):
    try:
        coords = (int(cmd_args[0]), int(cmd_args[1]))
        space = my_map.getMapSpace(coords)
        if space is not None:
            return str(space), None
        else:
            return None, 'Error: Invalid space'
    except ValueError: # Bad integer value
        return None, 'Error: Invalid integer value'
    except IndexError: # Not enough args
        return None, 'Error: Need both X and Y values\nUsage: SCPI [X] [Y]'

def parseMOVE(my_map, cmd_args):
    try:
        actor = cmd_args[0]
        coords = (int(cmd_args[1]), int(cmd_args[2]))

        actors = my_map.getActors()
        space = my_map.getMapSpace(coords)
        if actor not in actors:
            return None, 'Error: Actor not found'
        if space is None:
            return None, 'Error: Invalid space'
        return [actor, coords[0], coords[1]], None

    except ValueError: # Bad int
        return None, 'Error: Invalid integer value'
    except IndexError: # Missing args
        return None, 'Error: Missing args\nUsage: MOVE [ACTOR] [X] [Y]'

CMDS = {
    'MAPI': None,
    'ACTI': parseACTI,
    'SPCI': parseSPCI,
    'MOVE': parseMOVE,
    'HELP': None,
    'QUIT': None
}
CMD_LEN = len(max(CMDS.keys(), key=len))

ACTIenabled = True

def parseCommand(raw_command, my_map):
    # Get command key phrase
    cmd_key = raw_command[:CMD_LEN].upper()
    # Check for command parser
    try:
        raw_args = [val.strip() for val in raw_command[CMD_LEN+1:].split(' ')]
        cmd_args, result = CMDS[cmd_key](my_map, raw_args)
        if cmd_args:
            return cmd_key, cmd_args
        else:
            return None, result
    # Or no parser needed
    except TypeError: # No parser command
        return cmd_key, None
    except KeyError: # Command not recognized
        return None, 'Invalid command, Get Gud.'
